package co.funcorp.feed.repository;

import co.funcorp.feed.property.RedisProperty;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

@Repository
@RequiredArgsConstructor
@Slf4j
public class RedisRepository implements FeedRepository {

    private final Jedis jedis;
    private final RedisProperty redisProperty;

    @Override
    public List<String> findBetween(int from, int to) {
        log.debug("Redis: ZRANGE {} {} {}", redisProperty.feedKey, from, to);
        return new ArrayList<>( jedis.zrange(redisProperty.feedKey, from, to));
    }
}
