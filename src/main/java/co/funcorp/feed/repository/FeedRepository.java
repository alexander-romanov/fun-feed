package co.funcorp.feed.repository;

import java.util.List;

public interface FeedRepository {

    List<String> findBetween(int from, int to);
}
