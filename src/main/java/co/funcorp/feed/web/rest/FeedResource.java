package co.funcorp.feed.web.rest;

import co.funcorp.feed.domain.FeedResponse;
import co.funcorp.feed.exception.InvalidRequestException;
import co.funcorp.feed.service.FeedService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.constraints.Positive;

@RestController("/feed")
@Validated
@RequiredArgsConstructor
@Slf4j
public class FeedResource {

    private final FeedService feedService;

    @GetMapping
    public FeedResponse getFeed(
            @RequestParam @Positive int limit,
            @RequestParam(required = false) @Positive Integer next,
            @RequestParam(required = false) @Positive Integer prev) {
        log.debug("Request: GET /feed {limit={}, next={}, prev={}}", limit, next, prev);
        if (areBothExist(next, prev)) {
            throw new InvalidRequestException("getFeed: next and prev must not be specified simultaneously");
        }

        return feedService.getFeed(limit, next, prev);
    }

    private boolean areBothExist(Integer first, Integer second) {
        return first != null && second != null;
    }
}
