package co.funcorp.feed.config;

import co.funcorp.feed.property.RedisProperty;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import redis.clients.jedis.Jedis;
import redis.embedded.RedisServer;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;
import java.util.stream.IntStream;

@Configuration
@RequiredArgsConstructor
@Slf4j
public class RedisConfig {

    private final RedisProperty redisProperty;
    private RedisServer server;
    private Jedis jedis;

    @Bean
    public Jedis jedis() {
        return jedis;
    }

    @PostConstruct
    public void start() throws IOException {
        log.info("Starting Redis server with port: {} (http)", redisProperty.port);
        server = new RedisServer(redisProperty.port);
        server.start();
        jedis = new Jedis();
        warmUp();
    }

    @PreDestroy
    public void tearDown() {
        log.info("Stopping Redis server");
        jedis.close();
        server.stop();
    }

    private void warmUp() {
        log.info("Generating test data");
        IntStream.range(0, redisProperty.feedLength)
                .forEach(v -> jedis.zadd(redisProperty.feedKey, v, "value_" + v));
    }
}
