package co.funcorp.feed.domain;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class Navigation {

    public final int prev;
    public final int next;
    public final boolean hasPrev;
    public final boolean hasNext;
}
