package co.funcorp.feed.domain;

import lombok.RequiredArgsConstructor;

import java.util.List;

@RequiredArgsConstructor
public class FeedResponse {

    public final List<String> items;
    public final Navigation navigation;
}
