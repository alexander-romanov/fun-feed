package co.funcorp.feed.property;

import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

@ConfigurationProperties("redis")
@Setter
public class RedisProperty {

    public int port;
    public String feedKey;
    public int feedLength;
}
