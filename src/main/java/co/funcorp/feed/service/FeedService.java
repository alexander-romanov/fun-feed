package co.funcorp.feed.service;

import co.funcorp.feed.domain.FeedResponse;
import co.funcorp.feed.domain.Navigation;
import co.funcorp.feed.repository.FeedRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class FeedService {

    private final FeedRepository feedRepository;

    public FeedResponse getFeed(
            int limit,
            @Nullable Integer next,
            @Nullable Integer prev) {
        if (next != null) return next(limit, next);
        if (prev != null) return prev(limit, prev);
        return first(limit);
    }

    private FeedResponse first(int limit) {
        return response(
                feedRepository.findBetween(0, limit),
                limit,
                0,
                limit);
    }

    private FeedResponse next(int limit, int next) {
        return response(
                feedRepository.findBetween(next, next + limit),
                limit,
                next - 1,
                next + limit);
    }

    private FeedResponse prev(int limit, int prev) {
        return response(
                feedRepository.findBetween(prev - (limit - 1), prev + 1),
                limit,
                prev - limit,
                prev + 1);
    }

    private FeedResponse response(
            List<String> values,
            int limit,
            int prev,
            int next) {
        boolean hasPrev = true;
        boolean hasNext = true;

        if (prev <= 0) {
            prev = -1;
            hasPrev = false;
        }

        if (values.size() <= limit) {
            next = -1;
            hasNext = false;
        } else {
            values.remove(values.size() - 1);
        }

        Navigation navigation = new Navigation(prev, next, hasPrev, hasNext);
        return new FeedResponse(values, navigation);
    }
}
