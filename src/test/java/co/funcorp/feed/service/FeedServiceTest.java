package co.funcorp.feed.service;

import co.funcorp.feed.domain.FeedResponse;
import co.funcorp.feed.domain.Navigation;
import co.funcorp.feed.repository.FeedRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class FeedServiceTest {

    @InjectMocks
    private FeedService service;

    @Mock private FeedRepository repository;

    @Test
    public void shouldReturnFirstPage() {
        // Given
        List<String> feed = values(11);
        when(repository.findBetween(0, 10)).thenReturn(feed);

        // When
        FeedResponse response = service.getFeed(10, null, null);

        // Then
        assertThat(response.items, is(feed));
        assertThat(response.items, hasSize(10));

        Navigation navigation = response.navigation;
        assertThat(navigation.prev, is(-1));
        assertThat(navigation.next, is(10));
        assertFalse(navigation.hasPrev);
        assertTrue(navigation.hasNext);
    }

    @Test
    public void shouldReturnNextPage() {
        // Given
        List<String> feed = values(11);
        when(repository.findBetween(10, 20)).thenReturn(feed);

        // When
        FeedResponse response = service.getFeed(10, 10, null);

        // Then
        assertThat(response.items, is(feed));
        assertThat(response.items, hasSize(10));

        Navigation navigation = response.navigation;
        assertThat(navigation.prev, is(9));
        assertThat(navigation.next, is(20));
        assertTrue(navigation.hasPrev);
        assertTrue(navigation.hasNext);
    }

    @Test
    public void shouldReturnPrevPage() {
        // Given
        List<String> feed = values(11);
        when(repository.findBetween(10, 20)).thenReturn(feed);

        // When
        FeedResponse response = service.getFeed(10, null, 19);

        // Then
        assertThat(response.items, is(feed));
        assertThat(response.items, hasSize(10));

        Navigation navigation = response.navigation;
        assertThat(navigation.prev, is(9));
        assertThat(navigation.next, is(20));
        assertTrue(navigation.hasPrev);
        assertTrue(navigation.hasNext);
    }

    @Test
    public void shouldReturnLastPage() {
        // Given
        List<String> feed = values(10);
        when(repository.findBetween(90, 100)).thenReturn(feed);

        // When
        FeedResponse response = service.getFeed(10, 90, null);

        // Then
        assertThat(response.items, is(feed));
        assertThat(response.items, hasSize(10));

        Navigation navigation = response.navigation;
        assertThat(navigation.prev, is(89));
        assertThat(navigation.next, is(-1));
        assertTrue(navigation.hasPrev);
        assertFalse(navigation.hasNext);
    }

    @Test
    public void shouldReturnWholeFeed() {
        // Given
        List<String> feed = values(100);
        when(repository.findBetween(0, 100)).thenReturn(feed);

        // When
        FeedResponse response = service.getFeed(100, null, null);

        // Then
        assertThat(response.items, is(feed));
        assertThat(response.items, hasSize(100));

        Navigation navigation = response.navigation;
        assertThat(navigation.prev, is(-1));
        assertThat(navigation.next, is(-1));
        assertFalse(navigation.hasPrev);
        assertFalse(navigation.hasNext);
    }

    @Test
    public void shouldReturnWholeFeedWithExtraLargeLimit() {
        // Given
        List<String> feed = values(100);
        when(repository.findBetween(0, 10000)).thenReturn(feed);

        // When
        FeedResponse response = service.getFeed(10000, null, null);

        // Then
        assertThat(response.items, is(feed));
        assertThat(response.items, hasSize(100));

        Navigation navigation = response.navigation;
        assertThat(navigation.prev, is(-1));
        assertThat(navigation.next, is(-1));
        assertFalse(navigation.hasPrev);
        assertFalse(navigation.hasNext);
    }

    private List<String> values(int number) {
        List<String> feed = new ArrayList<>();
        for (int i = 0; i < number; i++) {
            feed.add("value_" + i);
        }

        return feed;
    }
}
