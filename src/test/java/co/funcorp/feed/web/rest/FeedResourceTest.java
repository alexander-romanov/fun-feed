package co.funcorp.feed.web.rest;

import co.funcorp.feed.domain.FeedResponse;
import co.funcorp.feed.domain.Navigation;
import co.funcorp.feed.exception.InvalidRequestException;
import co.funcorp.feed.service.FeedService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest
public class FeedResourceTest {

    private FeedResource resource;

    @Autowired private MockMvc mvc;
    @MockBean private FeedService service;

    @Before
    public void setUp() {
        resource = new FeedResource(service);
    }

    @Test(expected = InvalidRequestException.class)
    public void shouldThrowExceptionIfBothNextAndPrevAreSpecified() {
        resource.getFeed(10, 10, 10);
    }

    @Test
    public void shouldReturnValidJsonResponse() throws Exception {
        // Given
        FeedResponse expected = new FeedResponse(
                List.of("1", "2", "3"),
                new Navigation(0, -1, true, false));

        when(service.getFeed(10, null, null)).thenReturn(expected);

        // When & Then
        mvc.perform(get("/feed").param("limit", "10"))
                .andExpect(status().isOk())
                .andExpect(header().string("content-type", "application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.items", hasSize(3)))
                .andExpect(jsonPath("$.navigation.prev", is(0)))
                .andExpect(jsonPath("$.navigation.next", is(-1)))
                .andExpect(jsonPath("$.navigation.hasPrev", is(true)))
                .andExpect(jsonPath("$.navigation.hasNext", is(false)));
    }
}
